<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="container-fluid">
      <div class="row">
        <div class="col-md-4">
          <div class="panel-default panel pointed-border panel-message-list">
            <div class="panel-body">
              <div class="row">
                  <div class="form-group form-inline">
                    <button class="btn btn-primary btn-compose-mail "><i class="fa fa-plus" aria-hidden="true"></i>&nbsp;New Message</button>
                    <div class="input-group">
                      <input type="text" class="form-control " placeholder="Search...">
                      <span class="input-group-btn ">
                        <button class="btn btn-default btn-search"  type="button"><i class="glyphicon glyphicon-search"></i></button>
                      </span>
                    </div>
                  </div>
              </div>
             <div class="row">
                <div class="col-md-12">
                    <div class="btn-group" role="group" id="inside-options">
                      <button type="button" class="btn btn-default btn-group-options drop-checkbox"><input id="select-all" type="checkbox">&nbsp;<i class="fa fa-angle-down" aria-hidden="true"></i></button>
                      <button type="button" class="btn btn-default btn-group-options btn-delete"><i class="fa fa-trash" aria-hidden="true"></i></button>
                      <button type="button" class="btn btn-default btn-group-options btn-refresh"><i class="fa fa-refresh" aria-hidden="true"></i></button>
                    </div>
                </div>
              </div>  
              <div class="row">
                <div class="col-md-12 messagelist">
                  <div class="table-responsive">
                    <table class="table table-striped table-borderless">
                      <tbody>
                        <tr>
                          <td><div class="checkbox checkbox-primary"></div>
                          <input id="" type="checkbox" class="checkbox-primary"></td>
                          <td><b>Jame Node</b></td>
                          <td><p>Ut tristique dapibus nibh, sed scelerisque magna vehicula a.</p></td>
                          <td><b>12:30</b></td>
                        </tr>
                        <tr>
                          <td><div class="checkbox"></div>
                          <input id="" type="checkbox"></td>
                          <td><b>Jame Node</b></td>
                          <td><p>Ut tristique dapibus nibh, sed scelerisque magna vehicula a.</p></td>
                          <td><b>12:30</b></td>
                        </tr>
                        <tr>
                          <td><div class="checkbox"></div>
                          <input id="" type="checkbox"></td>
                          <td><b>Jame Node</b></td>
                          <td><p>Ut tristique dapibus nibh, sed scelerisque magna vehicula a.</p></td>
                          <td><b>12:30</b></td>
                        </tr><tr>
                          <td><div class="checkbox"></div>
                          <input id="" type="checkbox"></td>
                          <td><b>Jame Node</b></td>
                          <td><p>Ut tristique dapibus nibh, sed scelerisque magna vehicula a.</p></td>
                          <td><b>12:30</b></td>
                        </tr><tr>
                          <td><div class="checkbox"></div>
                          <input id="" type="checkbox"></td>
                          <td><b>Jame Node</b></td>
                          <td><p>Ut tristique dapibus nibh, sed scelerisque magna vehicula a.</p></td>
                          <td><b>12:30</b></td>
                        </tr><tr>
                          <td><div class="checkbox"></div>
                          <input id="" type="checkbox"></td>
                          <td><b>Jame Node</b></td>
                          <td><p>Ut tristique dapibus nibh, sed scelerisque magna vehicula a.</p></td>
                          <td><b>12:30</b></td>
                        </tr><tr>
                          <td><div class="checkbox"></div>
                          <input id="" type="checkbox"></td>
                          <td><b>Jame Node</b></td>
                          <td><p>Ut tristique dapibus nibh, sed scelerisque magna vehicula a.</p></td>
                          <td><b>12:30</b></td>
                        </tr><tr>
                          <td><div class="checkbox"></div>
                          <input id="" type="checkbox"></td>
                          <td><b>Jame Node</b></td>
                          <td><p>Ut tristique dapibus nibh, sed scelerisque magna vehicula a.</p></td>
                          <td><b>12:30</b></td>
                        </tr><tr>
                          <td><div class="checkbox"></div>
                          <input id="" type="checkbox"></td>
                          <td><b>Jame Node</b></td>
                          <td><p>Ut tristique dapibus nibh, sed scelerisque magna vehicula a.</p></td>
                          <td><b>12:30</b></td>
                        </tr>
                        <tr>
                          <td><div class="checkbox"></div>
                          <input id="" type="checkbox"></td>
                          <td><b>Jame Node</b></td>
                          <td><p>Ut tristique dapibus nibh, sed scelerisque magna vehicula a.</p></td>
                          <td><b>12:30</b></td>
                        </tr><tr>
                          <td><div class="checkbox"></div>
                          <input id="" type="checkbox"></td>
                          <td><b>Jame Node</b></td>
                          <td><p>Ut tristique dapibus nibh, sed scelerisque magna vehicula a.</p></td>
                          <td><b>12:30</b></td>
                        </tr><tr>
                          <td><div class="checkbox"></div>
                          <input id="" type="checkbox"></td>
                          <td><b>Jame Node</b></td>
                          <td><p>Ut tristique dapibus nibh, sed scelerisque magna vehicula a.</p></td>
                          <td><b>12:30</b></td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>  
            </div>
          </div>
        </div>

        <div class="col-md-7">
          <div class="panel panel-default pointed-border panel-view-message">
            <div class="panel-heading">
                <h4>Jame Node</h4>
            </div>
            <div class="panel-body">
               <div class="row">
                <div class=" col-md-4 col-md-offset-8 msg-panel-btn">
                  <div class="form-group">
                    <button id="inbox-delete" class="btn pull-right btn-danger"><i class="fa fa-trash"></i>&nbsp;Delete</button>
                  </div>
                </div>
              </div>
              <div class="well well-blue">Ut tristique dapibus nibh, sed scelerisque magna vehicula a Ut tristique dapibus nibh, sed scelerisque magna vehicula aUt tristique dapibus nibh. Ut tristique dapibus nibh, sed scelerisque magna vehicula aUt tristique dapibus nibh.
              </div>

              <div id="txt-area">
                <textarea rows="5" cols="100" form="usrform" placeholder="Type your message here..."></textarea>
                  <div class="form-group">
                    <button id="inbox-send" class="btn pull-right btn-info btn-md">Send</button>
                  </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>