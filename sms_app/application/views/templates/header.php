<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>SMS Blast</title>

    <link href="<?= base_url();?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="<?= base_url();?>assets/css/styles.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url();?>assets/css/classes.css">
    <link href="<?= base_url();?>assets/css/font-awesome/css/font-awesome.min.css" rel="stylesheet">

  </head>
  <body>