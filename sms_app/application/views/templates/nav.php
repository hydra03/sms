<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<nav class="navbar navbar-default navbar-fixed-top">
      <div class="container-fluid">
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
          <ul class="nav navbar-nav">
            <li <?= isset($active) && $active =='inbox' ? 'class="active"': null ?>><a href="<?= base_url();?>index.php/pages/inbox"><i class="fa fa-inbox" aria-hidden="true"></i> &nbsp; Inbox&nbsp;<span class="badge">12</span></a></li>
            <li <?= isset($active) && $active =='sent' ? 'class="active"': null ?>><a href="<?= base_url();?>index.php/pages/sent"><i class="fa fa-paper-plane-o" aria-hidden="true"></i> &nbsp; Sent Items</a></li>
            <li <?= isset($active) && $active =='outbox' ? 'class="active"': null ?>><a href="<?= base_url();?>index.php/pages/outbox"><i class="fa fa-external-link" aria-hidden="true"></i> &nbsp; Outbox</a></li>
          </ul>
        </div>
      </div>
    </nav>