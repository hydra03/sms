<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Pages extends CI_Controller {

		// public function __construct()
  //       {
  //           parent::__construct();
  //           $this->load->helper('url');
  //           $this->load->helper('url_helper');
  //       }

        public function index(){
          	$this->inbox();
        }

        public function inbox(){

        	$data['active'] = 'inbox'; 
        	$this->load->view('templates/header');
        	$this->load->view('templates/nav', $data);
        	$this->load->view('pages/inbox');
			$this->load->view('templates/footer');
			$this->load->view('templates/end');
        }

         public function sent(){

         	$data['active'] = 'sent'; 
        	$this->load->view('templates/header');
        	$this->load->view('templates/nav', $data);
        	$this->load->view('pages/sent');
			$this->load->view('templates/footer');
			$this->load->view('templates/end');
        }

        public function outbox(){

        	$data['active'] = 'outbox'; 
        	$this->load->view('templates/header');
        	$this->load->view('templates/nav', $data);
        	$this->load->view('pages/outbox');
			$this->load->view('templates/footer');
			$this->load->view('templates/end');
        }

}

