<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Pages extends CI_Controller {

		// public function __construct()
  //       {
  //           parent::__construct();
  //           $this->load->helper('url');
  //           $this->load->helper('url_helper');
  //       }

        public function view($page = 'inbox'){
          if (!file_exists(APPPATH.'views/pages/'.$page.'.php')) {
            show_404();
          }

          $data['active'] = $page;

          $this->load->view('templates/header');
          $this->load->view('templates/nav', $data);
          $this->load->view('pages/'.$page.'-side');
          $this->load->view('pages/'.$page);
          $this->load->view('templates/footer');
        }

        // public function newmsg(){
        //   if(file_exists(APPPATH.'views/pages/newmessage.php')){
        //     $this->load->view('templates/header');
        //     $this->load->view('templates/nav', $data);
        //     $this->load->view('pages/'.$page.'-side');
        //     $this->load->view('pages/'.$page);
        //     $this->load->view('templates/footer');
        //   }

        // }

}

