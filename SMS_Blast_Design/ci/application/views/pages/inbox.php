


    <div class="col-md-7">
      <div class="panel panel-default pointed-border panel-view-message">
        <div class="panel-heading">
            <h4>Jame Node</h4>
        </div>
        <div class="panel-body">
           <div class="row">
            <div class=" col-md-4 col-md-offset-8 msg-panel-btn">
              <div class="form-group">
                <button id="inbox-delete" class="btn pull-right btn-danger"><i class="fa fa-trash"></i>&nbsp;Delete</button>
              </div>
            </div>
          </div>
          <div class="well well-blue">Ut tristique dapibus nibh, sed scelerisque magna vehicula a Ut tristique dapibus nibh, sed scelerisque magna vehicula aUt tristique dapibus nibh. Ut tristique dapibus nibh, sed scelerisque magna vehicula aUt tristique dapibus nibh.
          </div>

          <div id="txt-area">
            <textarea rows="5" cols="100" form="usrform" placeholder="Type your message here..."></textarea>
              <div class="form-group">
                <button id="inbox-send" class="btn pull-right btn-info btn-md">Send</button>
              </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>