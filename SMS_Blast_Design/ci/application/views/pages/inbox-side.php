<div class="container-fluid">
  <div class="row">
    <div class="col-md-4">
      <div class="panel-default panel pointed-border panel-message-list">
        <div class="panel-body">
          <div class="row">
              <div class="form-group form-inline">
                <a href="<?php echo base_url(); ?>pages/view/newmessage"><button class="btn btn-primary btn-compose-mail "><i class="fa fa-plus" aria-hidden="true"></i>&nbsp;New Message</button></a>
                <div class="input-group">
                  <input type="text" class="form-control" id="searchBar" onkeyup="searchFunction()"" placeholder="Search...">
                  <span class="input-group-btn ">
                    <button class="btn btn-default btn-search"  type="button"><i class="glyphicon glyphicon-search"></i></button>
                  </span>
                </div>
              </div>
          </div>
         <div class="row">
            <div class="col-md-12">
                <div class="btn-group" role="group" id="inside-options">
                  <button type="button" class="btn btn-default btn-group-options drop-checkbox"><input id="select-all" type="checkbox">&nbsp;<i class="fa fa-angle-down" aria-hidden="true"></i></button>
                  <button type="button" class="btn btn-default btn-group-options btn-delete"><i class="fa fa-trash" aria-hidden="true"></i></button>
                  <button type="button" class="btn btn-default btn-group-options btn-refresh"><i class="fa fa-refresh" aria-hidden="true"></i></button>
                </div>
            </div>
          </div>  
          <div class="row">
            <div class="col-md-12 messagelist">
              <div class="table-responsive">
                <table class="table table-borderless table-hover" id="messages">
                  <tbody>
                    <a href="<?= base_url();?>direct.html">
                    <tr><a href=""></a>
                      <td><div class="checkbox checkbox-primary"></div>
                      <input id="" type="checkbox" class="checkbox-primary"></td>
                      <td><b>Jame Node</b></td>
                      <td><p>Ut tristique dapibus nibh, sed scelerisque magna vehicula a.</p></td>
                      <td><b>12:30</b></td>
                    </tr>
                    </a>
                    <a href="#">
                    <tr>
                      <td><div class="checkbox checkbox-primary"></div>
                      <input id="" type="checkbox" class="checkbox-primary"></td>
                      <td><b>Hannah Baker</b></td>
                      <td><p>Ut tristique dapibus nibh, sed scelerisque magna vehicula a.</p></td>
                      <td><b>12:30</b></td>
                    </tr>
                    </a>
                    <tr>
                      <td><div class="checkbox"></div>
                      <input id="" type="checkbox"></td>
                      <td><b>Alice Hamber</b></td>
                      <td><p>Ut tristique dapibus nibh, sed scelerisque magna vehicula a.</p></td>
                      <td><b>12:30</b></td>
                    </tr>
                    <tr>
                      <td><div class="checkbox"></div>
                      <input id="" type="checkbox"></td>
                      <td><b>Jensen Creep</b></td>
                      <td><p>Ut tristique dapibus nibh, sed scelerisque magna vehicula a.</p></td>
                      <td><b>12:30</b></td>
                    </tr><tr>
                      <td><div class="checkbox"></div>
                      <input id="" type="checkbox"></td>
                      <td><b>Jame Node</b></td>
                      <td><p>Ut tristique dapibus nibh, sed scelerisque magna vehicula a.</p></td>
                      <td><b>12:30</b></td>
                    </tr><tr>
                      <td><div class="checkbox"></div>
                      <input id="" type="checkbox"></td>
                      <td><b>Jame Node</b></td>
                      <td><p>Ut tristique dapibus nibh, sed scelerisque magna vehicula a.</p></td>
                      <td><b>12:30</b></td>
                    </tr><tr>
                      <td><div class="checkbox"></div>
                      <input id="" type="checkbox"></td>
                      <td><b>Jame Node</b></td>
                      <td><p>Ut tristique dapibus nibh, sed scelerisque magna vehicula a.</p></td>
                      <td><b>12:30</b></td>
                    </tr><tr>
                      <td><div class="checkbox"></div>
                      <input id="" type="checkbox"></td>
                      <td><b>Jame Node</b></td>
                      <td><p>Ut tristique dapibus nibh, sed scelerisque magna vehicula a.</p></td>
                      <td><b>12:30</b></td>
                    </tr><tr>
                      <td><div class="checkbox"></div>
                      <input id="" type="checkbox"></td>
                      <td><b>Jame Node</b></td>
                      <td><p>Ut tristique dapibus nibh, sed scelerisque magna vehicula a.</p></td>
                      <td><b>12:30</b></td>
                    </tr><tr>
                      <td><div class="checkbox"></div>
                      <input id="" type="checkbox"></td>
                      <td><b>Jame Node</b></td>
                      <td><p>Ut tristique dapibus nibh, sed scelerisque magna vehicula a.</p></td>
                      <td><b>12:30</b></td>
                    </tr>
                    <tr>
                      <td><div class="checkbox"></div>
                      <input id="" type="checkbox"></td>
                      <td><b>Jame Node</b></td>
                      <td><p>Ut tristique dapibus nibh, sed scelerisque magna vehicula a.</p></td>
                      <td><b>12:30</b></td>
                    </tr><tr>
                      <td><div class="checkbox"></div>
                      <input id="" type="checkbox"></td>
                      <td><b>Jame Node</b></td>
                      <td><p>Ut tristique dapibus nibh, sed scelerisque magna vehicula a.</p></td>
                      <td><b>12:30</b></td>
                    </tr><tr>
                      <td><div class="checkbox"></div>
                      <input id="" type="checkbox"></td>
                      <td><b>Jame Node</b></td>
                      <td><p>Ut tristique dapibus nibh, sed scelerisque magna vehicula a.</p></td>
                      <td><b>12:30</b></td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>  
        </div>
      </div>
    </div>
