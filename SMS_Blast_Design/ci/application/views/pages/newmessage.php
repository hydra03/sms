<div class="col-md-8">
  <div class="panel panel-write-message">
    <div class="panel-body">
       <div class="row">
          <form class="form-inline">
          <div class="form-group">
            <label class="sendtxt">Send To:</label>
            <div class="sendto">
              <input type="text" class="form-control typetxt">
              <div class="btn-group">
                <button type="button" class="btn btn-primary">Sony</button>
                <button type="button" class="btn btn-primary" data-toggle="">
                  <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                </button>
              </div>
              <div class="btn-group">
                <button type="button" class="btn btn-primary">Sony</button>
                <button type="button" class="btn btn-primary" data-toggle="">
                  <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                </button>
              </div>
              <div class="btn-group">
                <button type="button" class="btn btn-primary">Sony</button>
                <button type="button" class="btn btn-primary" data-toggle="">
                  <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                </button>
              </div>

            </div>
            
          </div>  
          <button type="button" class="btn btn-default sendtobtn createdraft" data-toggle="modal" data-target="#modalDrafts"><i class="fa fa-external-link" aria-hidden="true"></i> &nbsp; Save to Drafts</button>
          <button type="button" class="btn btn-default sendtobtn createcont" data-toggle="modal" data-target="#modalContacts"><i class="fa fa-search" aria-hidden="true"></i> &nbsp; Contacts</button>
        </form>

          <form class="form-inline createtxtarea">
            <div class="form-group formmessage">  
              <label class="col-md-2 msglabel">Message:</label>
              <textarea class="form-control col-md-10" rows="10"></textarea>
            </div>
            <div class="msg-btn">
              <button type="submit" class="btn btn-default createsend"><i class="fa fa-share" aria-hidden="true"></i> &nbsp; Send</button>
              <button type="submit" class="btn btn-default createsave"><i class="fa fa-share-square-o" aria-hidden="true"></i> &nbsp; Save</button>
              <button type="submit" class="btn btn-default createdlt"><i class="fa fa-trash-o" aria-hidden="true"></i> &nbsp; Delete</button>
            </div>
          </form>
      </div>
      

    </div>
  </div>
</div>

<!-- ---------------------MODALS------------------------------ -->
<!-- Modal for Contacts -->
<div id="modalContacts" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-body">
        

        
        <body data-spy="scroll" data-target=".navbar" data-offset="50">

          <nav class="navbar navbar-inverse navbar-fixed-top ispy">
            <div class="container-fluid">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <form class="form-inline formsrch">
                <div class="form-group">
                  
                  <div class="input-group">
                    <div class="input-group-addon"><i class="fa fa-search" aria-hidden="true"></i></div>
                    <input type="text" id="srchInput" onkeyup="searchFunc()" class="form-control">
                  </div>
                </div>
              </form>
              <div>
                <div class="collapse navbar-collapse" id="myNavbar">
                  <ul class="nav navbar-nav">
                    <li><a href="#contA">A</a></li>
                    <li><a href="#contB">B</a></li>
                    <li><a href="#contC">C</a></li>
                    <li><a href="#contD">D</a></li>
                    <li><a href="#contE">E</a></li>
                    <li><a href="#contF">F</a></li>
                    <li><a href="#contG">G</a></li>
                    <li><a href="#contH">H</a></li>
                    <li><a href="#contI">I</a></li>
                    <li><a href="#contJ">J</a></li>
                    <li><a href="#contK">K</a></li>
                    <li><a href="#contL">L</a></li>
                    <li><a href="#contM">M</a></li>
                    <li><a href="#contN">N</a></li>
                    <li><a href="#contO">O</a></li>
                    <li><a href="#contP">P</a></li>
                    <li><a href="#contQ">Q</a></li>
                    <li><a href="#contR">R</a></li>
                    <li><a href="#contS">S</a></li>
                    <li><a href="#contT">T</a></li>
                    <li><a href="#contU">U</a></li>
                    <li><a href="#contV">V</a></li>
                    <li><a href="#contW">W</a></li>
                    <li><a href="#contX">X</a></li>
                    <li><a href="#contY">Y</a></li>
                    <li><a href="#contZ">Z</a></li>
                  </ul>
                </div>
              </div>
            </div>
          </nav>

          <div class="contList">
            <div id="contA" class="container-fluid">
              <h1>A</h1>
              <table class="contModal">
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Maria Anders</td>
                </tr>
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Francisco Chang</td>
                </tr>
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Roland Mendel</td>
                </tr>
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Helen Bennett</td>
                </tr>
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Yoshi Tannamuri</td>
                </tr>
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Giovanni Rovelli</td>
                </tr>
              </table>
            </div>
            <div id="contB" class="container-fluid">
              <h1>B</h1>
              <table class="contModal">
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Maria Anders</td>
                </tr>
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Francisco Chang</td>
                </tr>
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Roland Mendel</td>
                </tr>
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Helen Bennett</td>
                </tr>
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Yoshi Tannamuri</td>
                </tr>
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Giovanni Rovelli</td>
                </tr>
              </table>
            </div>
            <div id="contC" class="container-fluid">
              <h1>C</h1>
              <table class="contModal">
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Maria Anders</td>
                </tr>
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Francisco Chang</td>
                </tr>
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Roland Mendel</td>
                </tr>
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Helen Bennett</td>
                </tr>
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Yoshi Tannamuri</td>
                </tr>
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Giovanni Rovelli</td>
                </tr>
              </table>
            </div>
            <div id="contD" class="container-fluid">
              <h1>D</h1>
              <table class="contModal">
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Maria Anders</td>
                </tr>
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Francisco Chang</td>
                </tr>
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Roland Mendel</td>
                </tr>
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Helen Bennett</td>
                </tr>
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Yoshi Tannamuri</td>
                </tr>
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Giovanni Rovelli</td>
                </tr>
              </table>
            </div>
            <div id="contE" class="container-fluid">
              <h1>E</h1>
              <table class="contModal">
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Maria Anders</td>
                </tr>
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Francisco Chang</td>
                </tr>
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Roland Mendel</td>
                </tr>
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Helen Bennett</td>
                </tr>
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Yoshi Tannamuri</td>
                </tr>
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Giovanni Rovelli</td>
                </tr>
              </table>
            </div>
            <div id="contF" class="container-fluid">
              <h1>F</h1>
              <table class="contModal">
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Maria Anders</td>
                </tr>
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Francisco Chang</td>
                </tr>
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Roland Mendel</td>
                </tr>
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Helen Bennett</td>
                </tr>
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Yoshi Tannamuri</td>
                </tr>
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Giovanni Rovelli</td>
                </tr>
              </table>
            </div>
            <div id="contG" class="container-fluid">
              <h1>G</h1>
              <table class="contModal">
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Maria Anders</td>
                </tr>
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Francisco Chang</td>
                </tr>
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Roland Mendel</td>
                </tr>
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Helen Bennett</td>
                </tr>
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Yoshi Tannamuri</td>
                </tr>
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Giovanni Rovelli</td>
                </tr>
              </table>
            </div>
            <div id="contH" class="container-fluid">
              <h1>H</h1>
              <table class="contModal">
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Maria Anders</td>
                </tr>
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Francisco Chang</td>
                </tr>
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Roland Mendel</td>
                </tr>
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Helen Bennett</td>
                </tr>
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Yoshi Tannamuri</td>
                </tr>
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Giovanni Rovelli</td>
                </tr>
              </table>
            </div>
            <div id="contI" class="container-fluid">
              <h1>I</h1>
              <table class="contModal">
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Maria Anders</td>
                </tr>
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Francisco Chang</td>
                </tr>
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Roland Mendel</td>
                </tr>
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Helen Bennett</td>
                </tr>
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Yoshi Tannamuri</td>
                </tr>
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Giovanni Rovelli</td>
                </tr>
              </table>
            </div>
            <div id="contJ" class="container-fluid">
              <h1>J</h1>
              <table class="contModal">
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Maria Anders</td>
                </tr>
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Francisco Chang</td>
                </tr>
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Roland Mendel</td>
                </tr>
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Helen Bennett</td>
                </tr>
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Yoshi Tannamuri</td>
                </tr>
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Giovanni Rovelli</td>
                </tr>
              </table>
            </div>
            <div id="contK" class="container-fluid">
              <h1>K</h1>
              <table class="contModal">
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Maria Anders</td>
                </tr>
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Francisco Chang</td>
                </tr>
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Roland Mendel</td>
                </tr>
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Helen Bennett</td>
                </tr>
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Yoshi Tannamuri</td>
                </tr>
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Giovanni Rovelli</td>
                </tr>
              </table>
            </div>
            <div id="contL" class="container-fluid">
              <h1>L</h1>
              <table class="contModal">
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Maria Anders</td>
                </tr>
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Francisco Chang</td>
                </tr>
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Roland Mendel</td>
                </tr>
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Helen Bennett</td>
                </tr>
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Yoshi Tannamuri</td>
                </tr>
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Giovanni Rovelli</td>
                </tr>
              </table>
            </div>
            <div id="contM" class="container-fluid">
              <h1>M</h1>
              <table class="contModal">
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Maria Anders</td>
                </tr>
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Francisco Chang</td>
                </tr>
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Roland Mendel</td>
                </tr>
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Helen Bennett</td>
                </tr>
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Yoshi Tannamuri</td>
                </tr>
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Giovanni Rovelli</td>
                </tr>
              </table>
            </div>
            <div id="contN" class="container-fluid">
              <h1>N</h1>
              <table class="contModal">
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Maria Anders</td>
                </tr>
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Francisco Chang</td>
                </tr>
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Roland Mendel</td>
                </tr>
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Helen Bennett</td>
                </tr>
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Yoshi Tannamuri</td>
                </tr>
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Giovanni Rovelli</td>
                </tr>
              </table>
            </div>
            <div id="contO" class="container-fluid">
              <h1>O</h1>
              <table class="contModal">
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Maria Anders</td>
                </tr>
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Francisco Chang</td>
                </tr>
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Roland Mendel</td>
                </tr>
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Helen Bennett</td>
                </tr>
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Yoshi Tannamuri</td>
                </tr>
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Giovanni Rovelli</td>
                </tr>
              </table>
            </div>
            <div id="contP" class="container-fluid">
              <h1>P</h1>
              <table class="contModal">
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Maria Anders</td>
                </tr>
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Francisco Chang</td>
                </tr>
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Roland Mendel</td>
                </tr>
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Helen Bennett</td>
                </tr>
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Yoshi Tannamuri</td>
                </tr>
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Giovanni Rovelli</td>
                </tr>
              </table>
            </div>
            <div id="contQ" class="container-fluid">
              <h1>Q</h1>
              <table class="contModal">
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Maria Anders</td>
                </tr>
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Francisco Chang</td>
                </tr>
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Roland Mendel</td>
                </tr>
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Helen Bennett</td>
                </tr>
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Yoshi Tannamuri</td>
                </tr>
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Giovanni Rovelli</td>
                </tr>
              </table>
            </div>
            <div id="contR" class="container-fluid">
              <h1>R</h1>
              <table class="contModal">
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Maria Anders</td>
                </tr>
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Francisco Chang</td>
                </tr>
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Roland Mendel</td>
                </tr>
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Helen Bennett</td>
                </tr>
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Yoshi Tannamuri</td>
                </tr>
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Giovanni Rovelli</td>
                </tr>
              </table>
            </div>
            <div id="contS" class="container-fluid">
              <h1>S</h1>
              <table class="contModal">
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Maria Anders</td>
                </tr>
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Francisco Chang</td>
                </tr>
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Roland Mendel</td>
                </tr>
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Helen Bennett</td>
                </tr>
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Yoshi Tannamuri</td>
                </tr>
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Giovanni Rovelli</td>
                </tr>
              </table>
            </div>
            <div id="contT" class="container-fluid">
              <h1>T</h1>
              <table class="contModal">
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Maria Anders</td>
                </tr>
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Francisco Chang</td>
                </tr>
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Roland Mendel</td>
                </tr>
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Helen Bennett</td>
                </tr>
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Yoshi Tannamuri</td>
                </tr>
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Giovanni Rovelli</td>
                </tr>
              </table>
            </div>
            <div id="contU" class="container-fluid">
              <h1>U</h1>
              <table class="contModal">
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Maria Anders</td>
                </tr>
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Francisco Chang</td>
                </tr>
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Roland Mendel</td>
                </tr>
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Helen Bennett</td>
                </tr>
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Yoshi Tannamuri</td>
                </tr>
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Giovanni Rovelli</td>
                </tr>
              </table>
            </div>
            <div id="contV" class="container-fluid">
              <h1>V</h1>
              <table class="contModal">
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Maria Anders</td>
                </tr>
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Francisco Chang</td>
                </tr>
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Roland Mendel</td>
                </tr>
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Helen Bennett</td>
                </tr>
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Yoshi Tannamuri</td>
                </tr>
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Giovanni Rovelli</td>
                </tr>
              </table>
            </div>
            <div id="contW" class="container-fluid">
              <h1>W</h1>
              <table class="contModal">
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Maria Anders</td>
                </tr>
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Francisco Chang</td>
                </tr>
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Roland Mendel</td>
                </tr>
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Helen Bennett</td>
                </tr>
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Yoshi Tannamuri</td>
                </tr>
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Giovanni Rovelli</td>
                </tr>
              </table>
            </div>
            <div id="contX" class="container-fluid">
              <h1>X</h1>
              <table class="contModal">
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Maria Anders</td>
                </tr>
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Francisco Chang</td>
                </tr>
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Roland Mendel</td>
                </tr>
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Helen Bennett</td>
                </tr>
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Yoshi Tannamuri</td>
                </tr>
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Giovanni Rovelli</td>
                </tr>
              </table>
            </div>
            <div id="contY" class="container-fluid">
              <h1>Y</h1>
              <table class="contModal">
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Maria Anders</td>
                </tr>
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Francisco Chang</td>
                </tr>
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Roland Mendel</td>
                </tr>
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Helen Bennett</td>
                </tr>
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Yoshi Tannamuri</td>
                </tr>
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Giovanni Rovelli</td>
                </tr>
              </table>
            </div>
            <div id="contZ" class="container-fluid">
              <h1>Z</h1>
              <table class="contModal">
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Maria Anders</td>
                </tr>
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Francisco Chang</td>
                </tr>
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Roland Mendel</td>
                </tr>
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Helen Bennett</td>
                </tr>
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Yoshi Tannamuri</td>
                </tr>
                <tr>
                  <td><img src="<?php echo base_url(); ?>assets/img/face.png"></td>
                  <td>Giovanni Rovelli</td>
                </tr>
              </table>
            </div>
          </div>
      </div>
    </div>

  </div>
</div>
<!-- Modal Drafts -->
<div id="modalDrafts" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Modal Header</h4>
      </div>
      <div class="modal-body">
                      <div class="row">
            <div class="col-md-12 messagelist">
              <div class="table-responsive">
                <table class="table table-borderless table-hover">
                  <tbody>
                    <tr>
                      <td><b>Jame Node</b></td>
                      <td><p>Ut tristique dapibus nibh, sed scelerisque magna vehicula a.</p></td>
                      <td><b>12:30</b></td>
                    </tr>
                    <tr>
                      <td><b>Jame Node</b></td>
                      <td><p>Ut tristique dapibus nibh, sed scelerisque magna vehicula a.</p></td>
                      <td><b>12:30</b></td>
                    </tr>
                    <tr>
                      <td><b>Jame Node</b></td>
                      <td><p>Ut tristique dapibus nibh, sed scelerisque magna vehicula a.</p></td>
                      <td><b>12:30</b></td>
                    </tr>
                    <tr>
                      <td><b>Jame Node</b></td>
                      <td><p>Ut tristique dapibus nibh, sed scelerisque magna vehicula a.</p></td>
                      <td><b>12:30</b></td>
                    </tr>
                    <tr>
                      <td><b>Jame Node</b></td>
                      <td><p>Ut tristique dapibus nibh, sed scelerisque magna vehicula a.</p></td>
                      <td><b>12:30</b></td>
                    </tr>
                    <tr>
                      <td><b>Jame Node</b></td>
                      <td><p>Ut tristique dapibus nibh, sed scelerisque magna vehicula a.</p></td>
                      <td><b>12:30</b></td>
                    </tr>
                    <tr>
                      <td><b>Jame Node</b></td>
                      <td><p>Ut tristique dapibus nibh, sed scelerisque magna vehicula a.</p></td>
                      <td><b>12:30</b></td>
                    </tr>
                    <tr>
                      <td><b>Jame Node</b></td>
                      <td><p>Ut tristique dapibus nibh, sed scelerisque magna vehicula a.</p></td>
                      <td><b>12:30</b></td>
                    </tr>
                    <tr>
                      <td><b>Jame Node</b></td>
                      <td><p>Ut tristique dapibus nibh, sed scelerisque magna vehicula a.</p></td>
                      <td><b>12:30</b></td>
                    </tr>
                    <tr>
                      <td><b>Jame Node</b></td>
                      <td><p>Ut tristique dapibus nibh, sed scelerisque magna vehicula a.</p></td>
                      <td><b>12:30</b></td>
                    </tr>
                    <tr>
                      <td><b>Jame Node</b></td>
                      <td><p>Ut tristique dapibus nibh, sed scelerisque magna vehicula a.</p></td>
                      <td><b>12:30</b></td>
                    </tr>
                    <tr>
                      <td><b>Jame Node</b></td>
                      <td><p>Ut tristique dapibus nibh, sed scelerisque magna vehicula a.</p></td>
                      <td><b>12:30</b></td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>  
      </div>
      
    </div>

  </div>
</div>