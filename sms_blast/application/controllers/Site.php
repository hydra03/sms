<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Site extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->helper('url');
		$this->inbox();
	}

	public function inbox()
	{
		$this->load->view('templates/header');
		$this->load->view('templates/nav');
		$this->load->view('pages/inbox_content');
		$this->load->view('templates/footer');
	}

	public function outbox()
	{
		$this->load->view('templates/header');
		$this->load->view('templates/nav');
		$this->load->view('pages/outbox_content');
		$this->load->view('templates/footer');
	}

	public function sent_items()
	{
		$this->load->view('templates/header');
		$this->load->view('templates/nav');
		$this->load->view('pages/sent-items_content');
		$this->load->view('templates/footer');
	}

	public function create()
	{
		$this->load->view('templates/header');
		$this->load->view('templates/nav');
		$this->load->view('pages/create');
		$this->load->view('templates/footer');
	}
}
