<nav class="navbar navbar-default navbar-fixed-top">
  <div class="container-fluid">       
        <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
          <!-- <button class="btn btn-default navbar-btn"><i class="fa fa-plus" aria-hidden="true"></i> &nbsp; Compose New Message</button> -->
      <ul class="nav navbar-nav">
        <li><a href="<?= base_url();?>index.php/site/inbox"><i class="fa fa-inbox" aria-hidden="true"></i> &nbsp; Inbox&nbsp;<span class="badge">12</span></a></li>
        <li><a href="<?= base_url();?>index.php/site/sent_items"><i class="fa fa-paper-plane-o" aria-hidden="true"></i> &nbsp; Sent Items</a></li>
        <li><a href="<?= base_url();?>index.php/site/outbox"><i class="fa fa-external-link" aria-hidden="true"></i> &nbsp; Outbox</a></li>
      </ul>
          
    </div>
  </div>
</nav>